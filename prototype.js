let Parser = require("rss-parser");
const express = require("express");
const app = express();
const port = 3000;

let parser = new Parser();

const sites = [
  "http://expressen.se/rss/nyheter",
  "http://gt.se/rss/nyheter",
  "http://kvp.se/rss/nyheter",
  "http://expressen.se/rss/sport",
  "http://expressen.se/rss/noje",
  "http://expressen.se/rss/debatt",
  "http://expressen.se/rss/ledare",
  "http://expressen.se/rss/kultur",
  "http://expressen.se/rss/ekonomi",
  "http://expressen.se/rss/halsa",
  "http://expressen.se/rss/levabo",
  "http://expressen.se/rss/motor",
  "http://expressen.se/rss/res",
  "http://expressen.se/rss/dokument"
];

const getArticles = async () => {
  const items = sites.map(s => {
    return parser.parseURL(s).then(res => res.items);
  });

  let resolvedArticles = [];

  await Promise.allSettled(items).then(results =>
    results.forEach(result => {
      if (result.status === "fulfilled") {
        resolvedArticles = resolvedArticles.concat(result.value);
      }
    })
  );

  const sortedArticles = resolvedArticles.sort(
    (a, b) => Date.parse(b.pubDate) - Date.parse(a.pubDate)
  );

  const uniqueGuid = [];

  const uniqueSortedArticles = sortedArticles.filter(article => {
    if (!uniqueGuid.includes(article.guid)) {
      uniqueGuid.push(article.guid);
      return true;
    }
  });


  return uniqueSortedArticles
    .slice(0, 10)
    .map(article => `<li><a href='${article.link}'>${article.title}</a></li>`)
    .join("");
};

app.get("/", async (req, res) => {
  const articles = await getArticles();
  res.send(`<ol>${articles}</ol>`);
});

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});

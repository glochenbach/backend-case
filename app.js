import express from "express";
import { RssService } from "./backend/rss.mjs";

const sites = [
  "http://expressen.se/rss/nyheter",
  "http://gt.se/rss/nyheter",
  "http://kvp.se/rss/nyheter",
  "http://expressen.se/rss/sport",
  "http://expressen.se/rss/noje",
  "http://expressen.se/rss/debatt",
  "http://expressen.se/rss/ledare",
  "http://expressen.se/rss/kultur",
  "http://expressen.se/rss/ekonomi",
  "http://expressen.se/rss/halsa",
  "http://expressen.se/rss/levabo",
  "http://expressen.se/rss/motor",
  "http://expressen.se/rss/res",
  "http://expressen.se/rss/dokument"
];
const app = express();
const port = 3000;
const fetcher = new RssService({ sites, limit: 5 });

app.get("/", async (req, res) => {
  let articles = await fetcher.getArticles(0, 10);
  articles = articles.map(
    article => `<li><a href='${article.link}'>${article.title}</a></li>`
  );
  res.send(`
    <ol>${articles.join("")}</ol>
    <i>Last updated: ${fetcher.lastUpdated}</i>
  `);
});

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});

# Bonnier Backend Case

The rss backend fetcher will collect and cache all articles in a list of rss-feeds.  
The last updated timestamp determines the last fetch.  
The cache will reload every 30 minutes.

![localhost](localhost.png)

## Getting started

    git clone https://bitbucket.org/glochenbach/backend-case.git
    npm i
    npm run start

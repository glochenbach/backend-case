import mockRssParser from "rss-parser";
import rss, { RssService } from "./rss.mjs";

jest.mock("rss-parser");

describe("handleResponse", () => {
  let handleResponse;

  beforeAll(() => {
    handleResponse = rss.__get__("handleResponse");
  });

  test("Should return empty result if empty is given", () => {
    expect(handleResponse([])).toEqual(expect.arrayContaining([]));
  });

  test("Should only return fulfilled promises", () => {
    const response = [
      {
        status: "fulfilled",
        value: ["success1"]
      },
      {
        status: "rejected",
        value: ["rejected1"]
      },
      {
        status: "fulfilled",
        value: ["success2"]
      },
      {
        status: "rejected",
        value: ["rejected2"]
      }
    ];

    const expected = ["success1", "success2"];

    expect(handleResponse(response)).toEqual(expect.arrayContaining(expected));
  });
});

describe("parseArticles", () => {
  let parseArticles;

  beforeAll(() => {
    parseArticles = rss.__get__("parseArticles");
  });

  test("Should remove all duplicate articles", () => {
    const articles = [
      {
        title: "Testttile aaa",
        pubDate: "Mon, 04 Jan 2021 15:26:05 +0100",
        guid: "aaa"
      },
      {
        title: "Testttile aaa",
        pubDate: "Mon, 04 Jan 2021 15:26:05 +0100",
        guid: "aaa"
      },
      {
        title: "Testttile aaa",
        pubDate: "Mon, 04 Jan 2021 15:26:05 +0100",
        guid: "aaa"
      },
      {
        title: "Testttile bbb",
        pubDate: "Mon, 04 Jan 2021 15:26:05 +0100",
        guid: "bbb"
      },
      {
        title: "Testttile bbb",
        pubDate: "Mon, 04 Jan 2021 15:26:05 +0100",
        guid: "bbb"
      },
      {
        title: "Testttile ccc",
        pubDate: "Mon, 04 Jan 2021 15:26:05 +0100",
        guid: "ccc"
      }
    ];

    expect(parseArticles(articles).length).toEqual(3);
  });

  test("Should sort articles descending", () => {
    const articles = [
      {
        title: "Testttile aaa",
        pubDate: "Mon, 04 Jan 2015 15:26:05 +0100",
        guid: "aaa"
      },
      {
        title: "Testttile bbb",
        pubDate: "Mon, 04 Jan 2021 15:26:05 +0100",
        guid: "bbb"
      },
      {
        title: "Testttile ccc",
        pubDate: "Mon, 04 Jan 2021 14:26:05 +0100",
        guid: "ccc"
      },
      {
        title: "Testttile ddd",
        pubDate: "Mon, 03 Jan 2021 15:26:05 +0100",
        guid: "ddd"
      }
    ];
    const result = parseArticles(articles);
    expect(result[0].guid).toEqual("bbb");
    expect(result[1].guid).toEqual("ccc");
    expect(result[2].guid).toEqual("ddd");
    expect(result[3].guid).toEqual("aaa");
  });
});

describe("RssService", () => {
  beforeEach(() => {
    mockRssParser.mockClear();
  });

  test("Should throw if no sites are given", () => {
    expect(() => {
      new RssService();
    }).toThrow();
  });

  test("Should fetch articles from 1 site", async () => {
    const response = {
      items: [
        {
          title: "Testttile aaa",
          pubDate: "Mon, 04 Jan 2015 15:26:05 +0100",
          guid: "aaa"
        },
        {
          title: "Testttile bbb",
          pubDate: "Mon, 04 Jan 2021 15:26:05 +0100",
          guid: "bbb"
        },
        {
          title: "Testttile ccc",
          pubDate: "Mon, 04 Jan 2021 14:26:05 +0100",
          guid: "ccc"
        },
        {
          title: "Testttile ddd",
          pubDate: "Mon, 03 Jan 2021 15:26:05 +0100",
          guid: "ddd"
        }
      ]
    };

    const mockParseURL = jest.fn().mockResolvedValueOnce(response);

    mockRssParser.mockImplementation(() => {
      return {
        parseURL: mockParseURL
      };
    });

    const service = new RssService({ sites: ["https://site1.com"] });
    const articles = await service.getArticles();

    expect(mockParseURL).toHaveBeenCalledTimes(1);
    expect(articles.length).toEqual(4);
  });

  test("Should fetch articles from 2 site", async () => {
    const response1 = {
      items: [
        {
          title: "Testttile aaa",
          pubDate: "Mon, 04 Jan 2015 15:26:05 +0100",
          guid: "aaa"
        }
      ]
    };

    const response2 = {
      items: [
        {
          title: "Testttile bbb",
          pubDate: "Mon, 04 Jan 2021 15:26:05 +0100",
          guid: "bbb"
        }
      ]
    };

    const mockParseURL = jest
      .fn()
      .mockResolvedValueOnce(response1)
      .mockResolvedValueOnce(response2);

    mockRssParser.mockImplementation(() => {
      return {
        parseURL: mockParseURL
      };
    });

    const service = new RssService({
      sites: ["https://site1.com", "https://site2.com"]
    });
    const articles = await service.getArticles();

    expect(mockParseURL).toHaveBeenCalledTimes(2);
    expect(articles.length).toEqual(2);
  });

  test("Should remove duplicate articles", async () => {
    const response1 = {
      items: [
        {
          title: "Testttile aaa",
          pubDate: "Mon, 04 Jan 2015 15:26:05 +0100",
          guid: "aaa"
        },
        {
          title: "Testttile bbb",
          pubDate: "Mon, 04 Jan 2021 15:26:05 +0100",
          guid: "bbb"
        },
        {
          title: "Testttile ccc",
          pubDate: "Mon, 04 Jan 2021 14:26:05 +0100",
          guid: "ccc"
        },
        {
          title: "Testttile ddd",
          pubDate: "Mon, 03 Jan 2021 15:26:05 +0100",
          guid: "ddd"
        }
      ]
    };

    const response2 = {
      items: [
        {
          title: "Testttile aaa",
          pubDate: "Mon, 04 Jan 2015 15:26:05 +0100",
          guid: "aaa"
        },
        {
          title: "Testttile bbb",
          pubDate: "Mon, 04 Jan 2021 15:26:05 +0100",
          guid: "bbb"
        },
        {
          title: "Testttile eee",
          pubDate: "Mon, 04 Jan 2021 15:26:05 +0100",
          guid: "eee"
        }
      ]
    };

    const mockParseURL = jest
      .fn()
      .mockResolvedValueOnce(response1)
      .mockResolvedValueOnce(response2);

    mockRssParser.mockImplementation(() => {
      return {
        parseURL: mockParseURL
      };
    });

    const service = new RssService({
      sites: ["https://site1.com", "https://site2.com"]
    });
    const articles = await service.getArticles();

    expect(mockParseURL).toHaveBeenCalledTimes(2);
    expect(articles.length).toEqual(5);
  });
});

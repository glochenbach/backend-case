import Parser from "rss-parser";

/**
 * Service to fetch and store rss feeds.
 *
 * @param {Object} options
 * @param {Array} options.sites an array of linkt to rss-feeds
 * @param {Number} options.refreshRate integer of seconds between the fetching is reloaded
 *
 */
export class RssService {
  #parser;

  constructor(options = {}) {
    if (!Array.isArray(options.sites) || options.sites.length < 1) {
      throw new ReferenceError("Sites must be deined as an array of sites");
    }

    this.#parser = new Parser();

    this.articles;

    this.refreshRate = options.refreshRate || 1800;
    this.sites = {
      sources: options.sites,
      //prepartation to retry offline sites
      online: [],
      offline: []
    };
    this.lastUpdated;

    this.fetchArticles();
    setInterval(this.fetchArticles.bind(this), this.refreshRate * 1000);
  }

  fetchArticles() {
    this.sites.online.length = 0;
    this.sites.offline.length = 0;

    const unresolvedSites = this.sites.sources.map(site =>
      this.#parser
        .parseURL(site)
        .then(res => {
          this.sites.online.push(site);
          return res.items;
        })
        .catch(e => {
          this.sites.offline.push(site);
          console.log(`Could not parse content from '${site}'`);
          throw e;
        })
    );

    this.articles = Promise.allSettled(unresolvedSites)
      .then(result => handleResponse(result))
      .then(result => parseArticles(result, this.offset, this.limit))
      .then(result => {
        this.lastUpdated = new Date();
        return result;
      });
  }

  /**
   * Get an array of articles as rss items
   *
   * @param {Number} offset where to start from the list
   * @param {Number} limit how many articles to return
   */
  async getArticles(offset = 0, limit = 10) {
    return this.articles.then(result => result.slice(offset, limit));
  }
}

/**
 * Normalize the rss-parsers reponse from a collection of promises
 *
 * @param {Array} response The Promise.allSettled wrapped response
 */
const handleResponse = response => {
  let result = response;
  result = result.filter(r => r.status === "fulfilled");
  result = result.map(r => r.value);
  result = result.flat();
  return result;
};

/**
 * Parse the rss articles and apply the comon business rules.
 *
 * @param {Array} response An array of rss articles
 * @returns {Array} A distinct ordered list of rss articles
 */
const parseArticles = response => {
  let result = response;

  const uniqueGuid = [];
  result = result.filter(article => {
    if (!uniqueGuid.includes(article.guid)) {
      uniqueGuid.push(article.guid);
      return true;
    }
    return false;
  });
  result = result.sort((a, b) => Date.parse(b.pubDate) - Date.parse(a.pubDate));

  return result;
};
